# Chk Deployment

This project contains the code for starting the entire Chk service with **Docker** or **Kubernetes**.

## Contents of the deployment
Chk deployed via `docker-compose` or Kubernetes contains the following:
1. Chk-server
2. Chk-next
7. A Mariadb  
8. Chk-docker-proxy